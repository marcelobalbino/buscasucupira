lista_artigo = []

def compara_listas(lista1=[], lista2=[]):
    #Lista dos artigos que possui um dos termos
    listaresul1 = []
    # Lista dos artigos que possui ambos termos
    listaresul2 = []
    p1 = 0
    p2 = 0

    #Verifica se uma das listas esta vazia
    if lista1 != None:
        acaboup1 = False
    else:
        acaboup1 = True

    if lista2 != None:
        acaboup2 = False
    else:
        acaboup2 = True

    while (not acaboup1) and (not acaboup2):
        #Ambos termos presentes
        if lista1[p1] == lista2[p2]:
            listaresul2.append(lista1[p1])
            p1=p1+1
            p2=p2+1
        # Somente primeiro termo presente
        elif lista1[p1] < lista2[p2]:
            listaresul1.append(lista1[p1])
            p1=p1+1
        # Somente segundo termo presente
        else:
            listaresul1.append(lista2[p2])
            p2=p2+1
        if p1 == len(lista1):
            acaboup1 = True
        if p2 == len(lista2):
            acaboup2 = True

    # Copia parte restante de lista2
    if not acaboup1:
        for i in range(p1, len(lista1)):
            listaresul1.append(lista1[i])
    #Copia parte restante de lista2
    if not acaboup2:
        for i in range(p2, len(lista2)):
            listaresul1.append(lista2[i])

    #print('Lista dos artigos onde ambos termos estão presentes \n')
    for i in range(0, len(listaresul2)):
        print(lista_artigo[listaresul2[i]])

    print('\n')
    #print('Lista dos artigos onde somente um dos termos está presentes \n')
    for i in range(0, len(listaresul1)):
        print(lista_artigo[listaresul1[i]])

def compara_listas2(matriz_termos=[]):
    #print('matriz de termos ', matriz_termos)
    #Armazena a posicao atual da coluna de cada termo
    posicao=[]
    matriz_artigos=[]

    #indica que a matriz foi toda percorrida
    fim = False
    for i in range(0, len(matriz_termos)):
        matriz_artigos.append([])
        if matriz_termos[i] != None:
            posicao.append(0)
        else:
            #Indica lista de artigos vazia para o termo
            posicao.append(-1)

    while fim == False:
        fim = True
        menor_artigo = -1

        #Procura a linha onde esta a menor posicao
        for i in range(0, len(matriz_termos)):
            #Verifica se ainda existe item a analisar
            if (posicao[i] != -1) and (posicao[i] < len(matriz_termos[i])):
                fim = False
                if menor_artigo == -1:
                    menor_artigo = matriz_termos[i][posicao[i]]
                if matriz_termos[i][posicao[i]] < menor_artigo:
                    menor_artigo = matriz_termos[i][posicao[i]]

        artigo_adicionado = menor_artigo

        if fim == False:
            qtd = 0
            for i in range(0, len(matriz_termos)):
                #print('i: ', i)
                if (posicao[i] != -1) and (posicao[i] < len(matriz_termos[i])):
                    if matriz_termos[i][posicao[i]] == menor_artigo:
                        qtd = qtd + 1
                        posicao[i] = posicao[i] + 1

            matriz_artigos[qtd-1].append(artigo_adicionado)

    #print('Lista dos artigos onde ambos termos estão presentes \n')
    #for i in (len(matriz_artigos)-1, -1, -1):
    #    for j in (len(matriz_artigos[i])-1, -1, -1):
    #for i in range(0, len(matriz_artigos)):
        #for j in range(0, len(matriz_artigos[i])):
            #print(lista_artigo[matriz_artigos[i][j]])
        #print('\n')

    i = len(matriz_artigos) - 1
    while i>= 0:
        for j in range(0, len(matriz_artigos[i])):
            print(lista_artigo[matriz_artigos[i][j]])
        print('\n')
        i = i - 1

class Artigo:
    def __init__(self, nome):
        self.nome = nome

class BSTNode:
    def __init__(self, key, pos_artigo, left=None, right=None):
        self.key = key
        self.artigo = []
        self.artigo.append(pos_artigo)
        self.left = left
        self.right = right

    def get(self, termo):

        if self.key:
            if termo == self.key:
                return self.artigo
            elif termo < self.key:
                if self.left:
                    return self.left.get(termo)
                else:
                     return None
            else:
                if self.right:
                    return self.right.get(termo)
                else:
                    return None
        else:
            return None


    def add(self, node):
        if node.key == self.key:
            self.artigo.append(node.artigo[0])
        elif node.key < self.key:
            if self.left is None:
                self.left = node
            else:
                self.left.add(node)
        else:
            if self.right is None:
                self.right = node
            else:
                self.right.add(node)

    def traverse(self):
        if self.key is None:
            return None
        else:
            if self.left:
                self.left.traverse()
            for i in range(0, len(self.artigo)):
                print(self.artigo[i])
            if self.right:
                self.right.traverse()

class BSTree:
    def __init__(self):
        self.root = BSTNode(None, None, None, None)
        self.root = None

    def get(self, termo):
        if self.root == None:
            return None
        else:
            return self.root.get(termo)

    def add(self, node):
        if self.root == None:
            self.root = node
        else:
            self.root.add(node)

    def traverse(self):
        self.root.traverse()

if __name__ == '__main__':
    #lista_artigo = []
    arv = BSTree()
    arqperiodicos = open('periodicosPUCMG.txt', 'r')
    periodicos = arqperiodicos.readline()
    while periodicos:
        periodicos = arqperiodicos.readline()
        periodicos = periodicos.replace('\n', '')
        periodicos = periodicos.replace('",', '')
        periodicos = periodicos.replace('"', '')
        artigo = periodicos
        lista_artigo.append(artigo)
        periodicos = periodicos.replace(',', '')
        periodicos = periodicos.replace('?', '')
        periodicos = periodicos.replace(':', '')
        periodicos = periodicos.split(' ')

        for i in range(1, len(periodicos)):
            termo = periodicos[i]
            if termo not in ('UM', 'A', 'AS', 'E', 'O', 'OS', 'DE', 'PARA', 'COM', 'SEM', 'EM', 'DA', 'DO', 'NÃO', 'AN', 'FROM', 'THE', 'AND', 'OR', 'TO', 'WITH', 'WITHOUT', 'ON', 'OF', 'FOR', 'IN'):
                novo = BSTNode(termo, len(lista_artigo)-1)
                arv.add(novo)

        for y in range(1, 5):
            periodicos = arqperiodicos.readline()

        periodicos = arqperiodicos.readline()
    arqperiodicos.close()

    #arv.traverse()
    print('Lista de artigos indexados')
    for i in range(0, len(lista_artigo)):
        print(i+1, ' - ', lista_artigo[i])

    entrada = input("Digite os termos a serem pesquisados: ")
    entrada = entrada.split(' ')
    procura1 = entrada[0]
    procura2 = entrada[1]
    #procura2 = input("Digite o segundo termo: ")

    B =[]
    #B.append([])
    for i in range(0, len(entrada)):
        lista_termo1 = arv.get(entrada[i])
        print('TERMO ', entrada[i])
        print('LISTA DO TERMO ', lista_termo1)
        B.append(lista_termo1)
        #print('entrada ', B[len(B)-1][i])

    print('original', B)
    compara_listas2(B)
    #lista_termo1 = arv.get(procura1)
    #lista_termo2 = arv.get(procura2)

    #print('Lista dos artigos onde ambos termos estão presentes \n')
    #for i in range(0, len(lista_termo1)):
    #    print(lista_termo1[i])
    #print('\n')
    #print('Lista dos artigos onde somente um dos termos está presentes \n')
    #for i in range(0, len(lista_termo2)):
    #    print(lista_termo2[i])

    #compara_listas(lista_termo1, lista_termo2)

#    A = []
 #   for i in range(5):
  #      A = A + [[0] * 5]  # cria uma nova lista [0]*5

